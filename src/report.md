## 1. Установка операционки

- cat /etc/issue

![cringe text](screens/1.cat.png?raw=true)

## 2. Создание юзера

- sudo adduser <user_name>

![cringe text](screens/2.user_create.png?raw=true)

- cat /etc/passwd

![cringe text](screens/2.cat_passwd.png?raw=true)

## 3. Натсройка сети ОС

- Задали имя машины

![cringe text](screens/3.rename.png?raw=true)

- Установка Московского времени

![cringe text](screens/3.time.png?raw=true)

- Отображение подключений
    lo - loopback interface, виртульный интерфейс, который всегда доступен, нужен для диагностики

![cringe text](screens/3.connections.png?raw=true)

- Выводим ip адресс машины

![cringe text](screens/3.ip.png?raw=true)

- Для работы по сети любому устройству требуется IP-адрес. В протоколе IPv4 это числовой идентификатор, состоящий из 4 разрядов, каждый из которых отделяется точкой, без него устройство не может быть определено в сетевой инфраструктуре. \
Прикладной протокол DHCP выполняет всю работу по подбору сетевых настроек автоматически, без необходимости присваивать вручную каждому устройству свой IP-адрес. Это очень упрощает работу системного администратора в случае расширения сети.

- Адреса шлюзов

![cringe text](screens/3.netstat.png?raw=true)

- До установки

![cringe text](screens/3.before.png?raw=true)

- После установки

![cringe text](screens/3.after.png?raw=true)

- Пинги

![cringe text](screens/3.pings.png?raw=true)

## 4. Обновление системы

- До апдейта

![cringe text](screens/4.before.png?raw=true)

- После sudo apt upgrade

![cringe text](screens/4.after.png?raw=true)

## 5. Использование судо

- По умолчанию, обычные пользователи имеют ограниченый доступ ко всем возможным действиям, чтобы снизить возможности пользователя создать проблемы в системе. Использование sudo дает возможность обойти эти ограничения, но по умолчанию судо не доступно обычным пользователям

![cringe text](screens/5.before_sudoing.png?raw=true)

- Выдаем права на использование sudo

![cringe text](screens/5.giving_sudo.png?raw=true)

- Теперь другой пользователь может использовать судо и имеет доступ ко всем возможностям

![cringe text](screens/5.after_sudoing.png?raw=true)

## 6. Синхронизация времени

![cringe text](screens/6.time.png?raw=true)

## 7. Установка текстовых редакторов

- vim. Чтобы войти в режиме ввода нажимаем 'i'. После завершения ввода esc, :wq

![cringe text](screens/7.vim1.png?raw=true)

- nano. после создания сразу вводим текст, ctrl+o - сохранить, ctrl+x - выйти

![cringe text](screens/7.nano1.png?raw=true)

- joe. после создания сразу вводим текст, ctrl+kx - выйти c сохранением

![cringe text](screens/7.joe1.png?raw=true)

- Заходим в файл, меняем текст и выходим без сохранения. vim - :q!

![cringe text](screens/7.vim2.png?raw=true)

- Заходим в файл, меняем текст и выходим без сохранения. nano - ctrl+x, n

![cringe text](screens/7.nano2.png?raw=true)

- Заходим в файл, меняем текст и выходим без сохранения. joe - ctrl+c, y

![cringe text](screens/7.joe2.png?raw=true)

- vim. Открываем файл, редактируем, ищем слово (:/crazy), заменяем слово (:s/crazy/luana). 

![cringe text](screens/7.vim3.png?raw=true)

![cringe text](screens/7.vim4.png?raw=true)

- nano. Открываем файл, редактируем, ищем слово (ctrl+w), заменяем слово (alt+r). 

![cringe text](screens/7.nano3.png?raw=true)

![cringe text](screens/7.nano4.png?raw=true)

- joe. Открываем файл, редактируем, ищем слово (ctrl+kf+enter), заменяем слово (r+enter). 

![cringe text](screens/7.joe3.png?raw=true)

![cringe text](screens/7.joe4.png?raw=true)

## 8. Установка и базовая настройка сервиса SSHD

- sudo apt install openssh-server - установка
- sudo systemctl enable ssh - автоматическая загрузка при запуске


- Чтобы изменить порт: открываем в тестовом редакторе sshd_config (sudo vim /etc/ssh/sshd/config), находим строку 'port 22' или '#port 22', выставляем нужный порт, раскомменчиваем строку, сохраняем и делаем ребут.

![cringe text](screens/8.autoenable.png?raw=true)

- ps aux | grep sshd - проверяем запущен ли sshd
    - Опция a указывает ps вывести на дисплей процессы всех пользователей, за исключением тех процессов, которые не связаны с терминалом и процессами группы лидеров.
    - В u — подставки для ориентированных на пользователя формате, который обеспечивает подробную информацию о процессах.
    - Опция x в ps перечисляет процессы без управляющего терминала. В основном это процессы, которые запускаются во время загрузки и работают в фоновом режиме.

![cringe text](screens/8.sshd_is_on.png?raw=true)

- netstat -tan. Netstat - позволяет 
    - -a - выводит информацию о всех сокетах(об активных которые слушают порты и неактивных, которые не слушают).
    - -n - Разрешать имена
    - -t - показвыает  tcp соединения

![cringe text](screens/8.netstat.png?raw=true)

## 9. Использование top и htop

- top
    - uptime - 2:05
    - 1 user
    - systemload
        - ![cringe text](screens/9.tasks.png?raw=true)
    - 94 процесса
    - нагрузка на CPU - 0%
    - нагрузка на память
        - ![cringe text](screens/9.memory.png?raw=true)
    - наибольшее использование памяти (shift+m - отсортировать по использованию памяти)
        - ![cringe text](screens/9.top_mem.png?raw=true)
    - наибольшее использование CPU (shift+p - отсортировать по использованию  CPU)
        - ![cringe text](screens/9.top_cpu.png?raw=true)

![cringe text](screens/9.top.png?raw=true)

- htop
    - сортировка по PID (F6, выбрать PID)
        - ![cringe text](screens/9.htop_pid.png?raw=true)
    - сортировка по использованию CPU (F6, выбрать PERCENT_CPU)
        - ![cringe text](screens/9.htop_cpu.png?raw=true)
    - сортировка по использованию памяти (F6, выбрать PERCENT_MEM)
        - ![cringe text](screens/9.htop_mem.png?raw=true)
    - сортировка по времени (F6, выбрать  TIME)
        - ![cringe text](screens/9.htop_time.png?raw=true)

![cringe text](screens/9.htop.png?raw=true)

## 10. Использование утилиты fdisk

![cringe text](screens/10.fdisk.png?raw=true)

- Название диска - VDOV HARDDISK
- Размер диска - 20.56 Gb
- Размер  swap - 

## 11. Использование утилиты df

- df
    - размер - 10218772, использовано - 4848412, свободно - 4829688, задействован 51%
- размер - 1К блоки

![cringe text](screens/11.df.png?raw=true)

- df -Th 
    - размер - 9.8G, использовано - 4.7G, свободно - 4.7G, задействован 51%
- Файловая система -  ext4

![cringe text](screens/11.dfth.png?raw=true)

## 12.  Использование утилиты du

- du -hs /home

    - ![cringe text](screens/12.home.png?raw=true)

- du -hs /var

    - ![cringe text](screens/12.var.png?raw=true)

- du -hs /var

    - ![cringe text](screens/12.varlog_all.png?raw=true)

- -h - запись памяти в человеческой записи, -s напечать общий размер папки

## 13. Использование утилиты ncdu

- ncdu -  вызов утилиты, навигация по папкам - стрелки

- Размер /home

    - ![cringe text](screens/13.ncdu_home.png?raw=true)

- Размер /var

    - ![cringe text](screens/13.ncdu_var.png?raw=true)

- Размер /var/log

    - ![cringe text](screens/13.ncdu_var_log.png?raw=true)

## 14.  Работа с логами

-  cat ../../var/log/dmesg

![cringe text](screens/14.dmesg.png?raw=true)

-  cat ../../var/log/syslog

![cringe text](screens/14.syslog.png?raw=true)

-  cat ../../var/log/auth.log

![cringe text](screens/14.lastlogin.png?raw=true)

- Перезапускаем sshd

![cringe text](screens/14.restart.png?raw=true)

![cringe text](screens/14.restart2.png?raw=true)

## 15. Использование планировщика заданий CRON

- Создание действия: crontab -e -> выбрать текстовый редактор (если не выбран) -> записать в одну строку временные установки, затем выполняемую команду
- crontab -l - просмотр текущих действий
- crontab -r - удаление действий

![cringe text](screens/15.syslog.png?raw=true)

![cringe text](screens/15.crontab.png?raw=true)

- после удаления

![cringe text](screens/15.after_remove.png?raw=true)